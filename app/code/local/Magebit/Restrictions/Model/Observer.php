<?php

/**
 * Magebit_Restrictions
 *
 * @category     Magebit
 * @package      Magebit_Restrictions
 * @author       Mairis Kimenis <info@magebit.com>
 * @copyright    Copyright (c) 2015 Magebit, Ltd.(http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Magebit_Restrictions_Model_Observer
{
	/**
	 * Checks if the called action is in the restricted list
	 * and shows a 404 error if it is
	 *
	 * @param $observer
	 * @return void
	 */
	public function controllerActionPredispatch( $observer )
	{
		/** @var Mage_Core_Controller_Front_Action $controller */
		$controller = $observer->getControllerAction();
		$action     = $controller->getFullActionName();

		// if it is not restricted, contionue as usually
		if ( !$this->isRestricted( $action ) ) {
			return;
		}

		/*
		 * The action is restricted, so we show a 404 error
		 */
		$request = $controller->getRequest();
		$request->initForward();
		$request->setControllerName( 'index' );
		$request->setModuleName( 'cms' );
		$request->setActionName( 'noRoute' )->setDispatched( false );
	}

	/**
	 * Checks whether the called action is in the restricted list
	 *
	 * @param $action
	 * @return bool
	 */
	protected function isRestricted( $action )
	{
		// do not restrict admin actions
		if ( strpos( $action, 'adminhtml_' ) === 0 ) {
			return false;
		}

		// get from config
		$restricted = array_filter( array_map( 'trim', array_merge(
			explode( ',', Mage::getStoreConfig( 'magebit_restrictions/general/restricted' ) ),
			explode( ',', Mage::getStoreConfig( 'magebit_restrictions/general/restricted_custom' ) )
		) ) );

		foreach ( $restricted as $restrictedAction ) {
			// found a restricted action
			if ( strpos( $action, $restrictedAction ) === 0 ) {
				return true;
			}
		}

		// no restricted actions found
		return false;
	}
}