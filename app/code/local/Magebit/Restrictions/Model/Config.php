<?php

/**
 * Magebit_Restrictions
 *
 * @category     Magebit
 * @package      Magebit_Restrictions
 * @author       Mairis Kimenis <info@magebit.com>
 * @copyright    Copyright (c) 2015 Magebit, Ltd.(http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Magebit_Restrictions_Model_Config
{
	/**
	 * Returns the restricted action options
	 *
	 * @return array
	 */
	protected function getRestrictedActions()
	{
		return array(
			'customer_account_index'   => Mage::helper( 'magebit_restrictions' )->__( 'Customer account page' ),
			'customer_account_edit'    => Mage::helper( 'magebit_restrictions' )->__( 'Edit customer account page' ),
			'customer_account_log'     => Mage::helper( 'magebit_restrictions' )->__( 'Customer login page' ),
			'customer_account_create'  => Mage::helper( 'magebit_restrictions' )->__( 'Registration page' ),
			'customer_account_confirm' => Mage::helper( 'magebit_restrictions' )->__( 'Customer confirm page' ),
			'customer_account_forgot'  => Mage::helper( 'magebit_restrictions' )->__( 'Forgot password page' ),
			'customer_account_reset'   => Mage::helper( 'magebit_restrictions' )->__( 'Change forgotten password page' ),
			'cms_page_view'            => Mage::helper( 'magebit_restrictions' )->__( 'CMS Pages' ),
			'checkout_cart_index'      => Mage::helper( 'magebit_restrictions' )->__( 'Cart page' ),
			'catalog_category'         => Mage::helper( 'magebit_restrictions' )->__( 'Category pages' ),
			'catalog_seo_sitemap'      => Mage::helper( 'magebit_restrictions' )->__( 'Sitemap' ),
			'catalogsearch_term'       => Mage::helper( 'magebit_restrictions' )->__( 'Search Terms' ),
			'catalogsearch_result'     => Mage::helper( 'magebit_restrictions' )->__( 'Simple search' ),
			'catalogsearch_advanced'   => Mage::helper( 'magebit_restrictions' )->__( 'Advanced search' ),
			'wishlist_'                => Mage::helper( 'magebit_restrictions' )->__( 'Wishlist' ),
		);
	}

	/**
	 * Options getter
	 *
	 * @return array
	 */
	public function toOptionArray()
	{
		$result = array();

		foreach ( $this->getRestrictedActions() as $value => $label ) {
			$result[] = compact( 'value', 'label' );
		}

		return $result;
	}

	/**
	 * Get options in "key-value" format
	 *
	 * @return array
	 */
	public function toArray()
	{
		return $this->getRestrictedActions();
	}
}